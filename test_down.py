import unittest
import inst_down

class TestIgDown(unittest.TestCase):
    def test1(self):
        '''
        Testing RE on URLs
        '''
        test_strings = ['https://www.instagram.com/p/C/',
        'https://www.instagram.com/p/BjA_hztgz8m/?utm_source=ig_web_copy_link',
        'https://www.instagram.com/p/BjA_hztgz8m/',
        'https://www.instagram.com/p/BjA_hztgz8m/?igshid=167xb7q1f3fzq',
        'https://www.instagram.com/p/BjA_hztgz8m/?utm_source=ig_web_button_share_sheet',
        'https://www.instagram.com/kevin/p/BjA_hztgz8m/']

        good_strings = ['https://www.instagram.com/p/C/',
        'https://www.instagram.com/p/BjA_hztgz8m/',
        'https://www.instagram.com/p/BjA_hztgz8m/',
        'https://www.instagram.com/p/BjA_hztgz8m/',
        'https://www.instagram.com/p/BjA_hztgz8m/',
        'https://www.instagram.com/kevin/p/BjA_hztgz8m/']

        self.assertEqual(len(test_strings), len(good_strings))
        i = len(test_strings) - 1
        while i >= 0:
            self.assertEqual(inst_down.clean_url(test_strings[i]), good_strings[i])
            #print(i)
            i -= 1

if __name__ == "__main__":
    unittest.main()
    # test_strings = ['https://www.instagram.com/p/C/',
    #     'https://www.instagram.com/p/BjA_hztgz8m/?utm_source=ig_web_copy_link',
    #     'https://www.instagram.com/p/BjA_hztgz8m/',
    #     'https://www.instagram.com/p/BjA_hztgz8m/?igshid=167xb7q1f3fzq',
    #     'https://www.instagram.com/p/BjA_hztgz8m/?utm_source=ig_web_button_share_sheet',
    #     'https://www.instagram.com/kevin/p/BjA_hztgz8m/']
    # i = len(test_strings) - 1
    # while i >= 0:
    #     print(inst_down.clean_url(test_strings[i]))
    #     i -= 1