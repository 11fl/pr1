import requests
import re
import json

class GetContent:
    '''
    Get content from given url
    '''

    add = '?__a=1'
    useragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'
    headers = {'User-Agent': useragent}

    
    def __init__(self, url):
        self.url = url
        self.final_url = ''
        self.type = None
    
    def content(self):
        """
        get url for image or video
        returns: dict 
            ok
            message
            url
        """
        r = requests.get(f'{self.url}{self.add}', headers=self.headers)
        rj = json.loads(r.text)
        if r.status_code == 404:
            resp = {
            'ok': False,
            'type': None,
            'url' : False
            }
            self.final_url = resp['url']
            return resp

        if rj['graphql']['shortcode_media']['__typename'] == "GraphVideo":
            
            resp = {
                'ok' : True,
                'type': 'mp4',
                'url' : rj['graphql']['shortcode_media']['video_url']
            }
            self.type = resp['type']
            self.final_url = resp['url']
            return resp
            

        if rj['graphql']['shortcode_media']['__typename'] == "GraphImage":
            resp = {
                'ok' : True,
                'type': 'jpg',
                'url' : rj['graphql']['shortcode_media']['display_resources'][2]['src']

            }
            self.type = resp['type']
            self.final_url = resp['url']
            return resp


    def get_clean_url(self):
        '''
        Re on url, to clean for json parsing
        '''
        pattern = r'(https:?\/\/www\.)?instagram\.com(\/p?\/?\w.+\/)?(\/?p\/([A-z0-9]*\/?))?'
        clean = re.search(pattern, self.url)

        self.url = clean.group(0)
    
    def save_file(self):
        if self.final_url != False and self.final_url != '':
            r = requests.get(self.final_url, stream=True, headers=self.headers) 

        #change path !!!
            with open(f"/programming/some_tests/image.{self.type}",'wb') as j:
                for chunk in r.iter_content(chunk_size=1024):
                    if chunk: 
                        j.write(chunk)
        else:
           print('no url')
        

if __name__ == "__main__":
    pass