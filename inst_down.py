import requests
import json
from pprint import pp
import re

add = '?__a=1'
useragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36'
headers = {'User-Agent': useragent}


def clean_url(DIRY_URL):
    pattern = r'(https:?\/\/www\.)?instagram\.com(\/p?\/?\w.+\/)?(\/?p\/([A-z0-9]*\/?))?'
    clean = re.search(pattern, DIRY_URL)
    #print(clean.group(0), '-----------------------------')
    try:
        return clean.group(0)
    except AttributeError as e:
        print(f'Some shit happens.\n Error is: {e}')
    return 'https://www.instagram.com/p/C/'

def get_image(url):
    r = requests.get(f'{url}{add}', headers=headers)
    #print(r.status_code)
    if r.status_code == 404:
        resp = {
            'ok': False,
            'message': 'Profile is private'
        }
        return resp
    rj = json.loads(r.text)
    #print(rj)
    resp = {
        'ok' : True,
        'message': 'Profile is open',
        'url' : rj['graphql']['shortcode_media']['display_resources'][2]['src']
    }
    #print(resp)
    #return rj['graphql']['shortcode_media']['display_resources'][2]['src']
    return resp

def save_image(image):
    #print(image)
    if image['ok'] == True:
        r = requests.get(image['url'], stream=True, headers=headers) 
    #change path !!!
        with open("/programming/some_tests/image.jpg",'wb') as j:
            for chunk in r.iter_content(chunk_size=1024):
                if chunk: 
                    j.write(chunk)
    print(image['message'])
    return image['message']

if __name__ == "__main__":
    pass