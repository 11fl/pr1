from flask import Flask
from inst_down import clean_url, get_image
from totg import sendPhoto

app = Flask(__name__) 

@app.route('/')
@app.route('/<path:imgurl>')
def index(imgurl=False):
    if imgurl:
        clean = clean_url(imgurl)
        #print(clean)
        #print(get_image(clean))
        return f'<img src="{get_image(clean)}">'
    return 'Just paste url of image after / in adress bar'

@app.route('/dkcjn394fu34hfcofidkf93ijf3')
def webhook():
    ...


if __name__ == "__main__":
    app.run(debug=True)