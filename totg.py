import requests
import os
import json
from pprint import pp
import inst_down


TMP_CHATID = os.getenv('CAHTID')
TOKEN = os.getenv("TGTOKEN")


API_URL=f'https://api.telegram.org/bot{TOKEN}'

def getMe(url, method):
    r = requests.get(f'{url}{method}')
    pp(json.loads(r.text))

def getUpdates(url, method='/getUpdates'):
    r = requests.get(f'{url}{method}')
    pp(json.loads(r.text))

def sendMessage(url, chatid, text, method='/sendMessage'):
    payload = {
        'chat_id': chatid,
        'text' : text
    }
    requests.post(f'{url}{method}', data=payload)

def sendPhoto(url, chatid, photourl, method='/sendPhoto'):
    if photourl['ok'] == False:
        payload = {
        'chat_id': chatid,
        'text': photourl['message']
    }
        requests.post(f'{API_URL}/sendMessage', data=payload)
        
    else:
        payload = {
            'chat_id': chatid,
            'photo': photourl['url']
        }
        
        requests.post(f'{API_URL}{method}', data=payload)
    #print()

if __name__ == "__main__":
    pass
